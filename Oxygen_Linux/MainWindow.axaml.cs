using Avalonia.Controls;
using Avalonia.Interactivity;
using Oxygen;
using System.Windows;

namespace Oxygen_Linux
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        
        public async void button_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var attachResult = await API.Inject();
            
            switch (attachResult)
            {
                case API.injectionResult.DLLBlocked:
                case API.injectionResult.RobloxNotFound:
                case API.injectionResult.InjectorBlocked:
                case API.injectionResult.InjectionFailed: 
                    return;
            }
        }
    }
}